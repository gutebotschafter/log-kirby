<?php snippet('header') ?>

<main class="page-main">
  <div class="page-main__content">
    <header class="page-main__content__title">
      <?php snippet("elements/breadcrumb") ?>
        <?php if($page->headline()->isNotEmpty()): ?>
          <h1><?= $page->headline()->html() ?></h1>
        <?php elseif($page->isHomePage()): ?>
          <h1><?= $site->title()->html() ?></h1>
        <?php else: ?>
          <h1><?= $page->title()->html() ?></h1>
        <?php endif ?>
        <?= $page->intro()->kirbytext() ?>
    </header>
    <?= $page->text()->kt() ?>
  </div>
  <?php foreach($page->children()->listed() as $department): ?>
    <?php if($department->children()->listed()->count() > 0): ?>
      <section class="page-main__employees">
        <header class="page-main__employees__title">
          <h2><?= $department->title() ?></h2>
        </header>
        <?php
          $employees = $department->children()->listed();
          if($department->orderASC() == 'true') {
            $employees = $employees->sortBy('title', 'asc');
          }
          foreach($employees as $item):
        ?>
          <article class="page-main__employees__item">
            <figure>
              <img src="<?= $item->picture()->toFile()->thumb(['width' => 480, 'height' => 320, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $item->picture()->toFile()->alt()->html() ?>">
            </figure>
            <p>
              <strong><?= $item->title()->html() ?></strong>
              <?= $item->description()->html() ?>
            </p>
          </article>
        <?php endforeach ?>
      </section>
    <?php endif ?>
  <?php endforeach ?>
</main>

<?php snippet("footer") ?>
