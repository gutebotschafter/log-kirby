<?php snippet('header') ?>

<main class="page-main">
  <div class="page-main__content">
    <header class="page-main__content__title">
      <?php snippet("elements/breadcrumb") ?>
        <?php if($page->headline()->isNotEmpty()): ?>
          <h1><?= $page->headline()->html() ?></h1>
        <?php elseif($page->isHomePage()): ?>
          <h1><?= $site->title()->html() ?></h1>
        <?php else: ?>
          <h1><?= $page->title()->html() ?></h1>
        <?php endif ?>
        <?= $page->intro()->kirbytext() ?>
    </header>
    <?= $page->text()->kt() ?>
  </div>
  <?php if($kirby->collection('projects')->listed()->count() > 0): ?>
    <div class="page-main__projects">
      <?php foreach($kirby->collection('projects')->listed() as $item): ?>
        <article class="page-main__projects__item">
          <a href="<?= $item->url() ?>" title="Das Projekt <?= $item->title()->html() ?> anzeigen">
            <figure>
              <img src="<?= $item->picture()->toFile()->thumb(['width' => 480, 'height' => 320, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $item->picture()->toFile()->alt()->html() ?>">
            </figure>
            <p>
              <strong><?= $item->title()->html() ?></strong>
              <?= $item->description()->html() ?>
            </p>
          </a>
        </article>
      <?php endforeach ?>
    </div>
  <?php endif ?>
</main>

<?php snippet("footer") ?>
