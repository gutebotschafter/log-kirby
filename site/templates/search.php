<?php snippet('header') ?>

<main class="page-main">
  <div class="page-main__content">
    <header class="page-main__content__title">
      <?php snippet("elements/breadcrumb") ?>
      <?php if (count($results) > 0): ?>
        <h1>Suchergebnisse</h2>
        <p>Ihre Suche nach <strong><?= html($query) ?></strong> hat <strong><?= count($results) ?></strong> Treffer ergeben.</p>
      <?php elseif(count($results) == 0 && html($query) !== ""): ?>
        <h1><?= $page->zerohitsheadline()->html() ?></h1>
        <?= $page->zerohitstext()->kt() ?>
      <?php else: ?>
        <h1><?= $page->title()->html() ?></h1>
        <?= $page->intro()->kt() ?>
      <?php endif ?>
    </header>
    <form class="form">
      <div class="form__set">
        <input class="form__text" type="search" name="q" value="<?= html($query) ?>">
        <input class="form__btn" type="submit" value="Suchen">
      </div>
    </form>
    <ol class="list-searchresults">
      <?php foreach ($results as $result): ?>
      <li>
        <a href="<?= $result->url() ?>">
          <h2><?= $result->title() ?></h2>
          <?php if($result->metaDescription()->isNotEmpty()): ?>
            <p><?= $result->metaDescription()->excerpt(256) ?></p>
          <?php elseif($result->description()->isNotEmpty()): ?>
            <p><?= $result->description()->excerpt(256) ?></p>
          <?php else: ?>
            <p><?= $result->intro()->excerpt(256) ?></p>
          <?php endif ?>
        </a>
      </li>
      <?php endforeach ?>
    </ol>
</main>

<?php snippet('footer') ?>
