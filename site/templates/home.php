<?php snippet('header') ?>

<main class="page-main">
  <div class="page-main__content">
    <header class="page-main__content__title">
      <?php snippet("elements/breadcrumb") ?>
        <?php if($page->headline()->isNotEmpty()): ?>
          <h1><?= $page->headline()->html() ?></h1>
        <?php elseif($page->isHomePage()): ?>
          <h1><?= $site->title()->html() ?></h1>
        <?php else: ?>
          <h1><?= $page->title()->html() ?></h1>
        <?php endif ?>
        <?= $page->intro()->kirbytext() ?>
    </header>
  </div>
  <?php if($page->cta()->isNotEmpty()): ?>
    <aside class="page-main__aside">
      <p>
        <?php foreach($page->cta()->toPages() as $item): ?>
          <a class="btn" href="<?= $item->url() ?>" title="Die Seite <?= $item->title()->html() ?> anzeigen"><?= $item->title()->html() ?></a>
        <?php endforeach ?>
      </p>
    </aside>
  <?php endif ?>
</main>

<?php snippet("footer") ?>
