<?php snippet('header') ?>

<main class="page-main">
  <div class="page-main__content">
    <header class="page-main__content__title">
      <?php snippet("elements/breadcrumb") ?>
        <?php if($page->headline()->isNotEmpty()): ?>
          <h1><?= $page->headline()->html() ?></h1>
        <?php elseif($page->isHomePage()): ?>
          <h1><?= $site->title()->html() ?></h1>
        <?php else: ?>
          <h1><?= $page->title()->html() ?></h1>
        <?php endif ?>
        <?= $page->intro()->kirbytext() ?>
    </header>
    <?= $page->text()->kt() ?>
  </div>
  <?php if($page->aside()->isNotEmpty()): ?>
    <aside class="page-main__aside">
      <?= $page->aside()->kirbytext() ?>
    </aside>
  <?php endif ?>
</main>

<?php snippet("footer") ?>
