<?php

return function ($site) {

  $query   = get('q');
  $results = $site->search($query, 'title|headline|text|intro|description')->published();

  return [
    'query'   => $query,
    'results' => $results,
  ];

};
