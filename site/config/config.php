<?php

return [
    'debug'  => false,
    'cache'  => true,
    'locale' => 'de_DE.utf-8',
    'smartypants' => true,
    "token" => "1b7afb63b2b4e12a6eb941b90be1f4b76fc8512df513581dsad",
    "environments" => [
        "production" => "https://logo-koeln.de",
        "stage" => "https://log.projektstatus.de",
        "virt" => "http://localhost"
    ]
];
