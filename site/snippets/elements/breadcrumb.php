<?php if($page->template() != 'home'): ?>
  <nav class="breadcrumb" role="navigation">
    <?php foreach($site->breadcrumb() as $crumb): ?>
      <a class="breadcrumb__item" href="<?= $crumb->url() ?>" title="<?= $crumb->title()->html() ?> anzeigen"><?= $crumb->title()->html() ?></a>
    <?php endforeach ?>
  </nav>
<?php endif ?>
