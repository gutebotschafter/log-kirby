<footer class="page-footer">
  <div class="page-footer__contact">
    <address class="page-footer__contact__item">
      <strong><?= $site->author()->html() ?></strong>
      <br>Telefon <?= $site->phone()->html() ?>
      <?php if($site->fax()->isNotEmpty()): ?>
        <br>Telefax <?= $site->fax()->html() ?>
      <?php endif ?>
      <br><a href="mailto:<?= $site->email()->html() ?>" title="Eine E-Mail an <?= $site->author()->html() ?> schreiben"><?= $site->email()->html() ?></a>
    </address>
    <?php foreach($site->content()->locations()->toStructure() as $entry):?>
      <address class="page-footer__contact__item">
        <strong>Standort <?= $entry->city()->html() ?></strong>
        <br><?= $entry->address()->html() ?>
        <br><?= $entry->zip()->html() ?> <?= $entry->city()->html() ?>
      </address>
    <?php endforeach ?>
  </div>
  <nav class="page-footer__menu" role="navigation">
    <?php foreach($site->menuMeta()->toPages() as $item): ?>
      <a class="page-footer__menu__item" href="<?= $item->url() ?>" title="Die Seite <?= $item->title()->html() ?> anzeigen"><?= $item->title()->html() ?></a>
    <?php endforeach ?>
  </nav>
</footer>
