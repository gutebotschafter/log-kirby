<nav class="page-menu" id="page-menu">
  <div class="page-menu__wrap">
    <?php if ($pages->listed()->count() > 0): ?>
      <div class="page-menu__mainmenu">
        <div class="page-menu__mainmenu__wrap">
          <?php foreach ($pages->listed() as $item): ?>
            <div class="page-menu__mainmenu__item">
              <a class="<?= e($item->isOpen(), 'active') ?>" href="<?= $item->url() ?>" title="Die Seite <?= $item->title()->html() ?> anzeigen"><?= $item->title()->html() ?></a>
              <?php if ($item->children()->listed()->count() > 0): ?>
                <div class="page-menu__submenu<?= e($item->isOpen(), ' page-menu__submenu--active') ?>">
                  <span class="page-menu__submenu__toggle"></span>
                  <div class="page-menu__submenu__wrap">
                    <?php foreach ($item->children()->listed() as $child): ?>
                      <div class="page-menu__submenu__item">
                        <a class="<?= e($child->isOpen(), 'active') ?>" href="<?= $child->url() ?>" title="Die Seite <?= $child->title()->html() ?> anzeigen"><?= $child->title()->html() ?></a>
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>
              <?php endif ?>
            </div>
          <?php endforeach ?>
          <?php if ($site->searchIcon()->toBool()): ?>
            <div class="page-menu__mainmenu__item">
              <a class="mobile" href="<?= $pages->find('suche')->url() ?>" title="Die Suche anzeigen"><?= $pages->find('suche')->title() ?></a>
              <a class="desktop" href="<?= $pages->find('suche')->url() ?>" title="Die Suche anzeigen"><?php snippet('svg/icons/search') ?></a>
            </div>
          <?php endif ?>
        </div>
      </div>
    <?php endif ?>
    <?php if($kirby->collection('languages')->listed()->count() > 0): ?>
      <div class="page-menu__languages">
        <?php foreach($kirby->collection('languages')->listed() as $item): ?>
          <a class="page-menu__languages__item" href="<?= $item->url() ?>" title="<?= $item->title()->html() ?>">
            <?php snippet('svg/languages/'. $item->languageCode()->html()) ?>
          </a>
        <?php endforeach ?>
      </div>
    <?php endif ?>
    <div class="page-menu__contact">
      <span class="page-menu__contact__item"><a href="tel: <?= $site->phone()->html() ?>" title="<?= $site->author()->html() ?> anrufen">Tel. <?= $site->phone()->html() ?></a></span>
      <span class="page-menu__contact__item"><a href="mailto:<?= $site->email()->html() ?>" title="<?= $site->author()->html() ?> eine E-Mail schreiben"><?= $site->email()->html() ?></a></span>
    </div>
  </div>
</nav>
