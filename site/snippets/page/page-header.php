<header class="page-header">
  <div class="page-header__wrapper">
    <?php if($kirby->collection('languages')->listed()->count() > 0): ?>
      <nav class="page-header__languages">
        <?php foreach($kirby->collection('languages')->listed() as $item): ?>
          <a class="page-header__languages__item" href="<?= $item->url() ?>" title="<?= $item->title()->html() ?>">
            <?php snippet('svg/languages/'. $item->languageCode()->html()) ?>
          </a>
        <?php endforeach ?>
      </nav>
    <?php endif ?>
    <div class="page-header__contact">
      <span class="page-header__contact__item"><a href="tel: <?= $site->phone()->html() ?>" title="<?= $site->author()->html() ?> anrufen">Tel. <?= $site->phone()->html() ?></a></span>
      <span class="page-header__contact__item"><a href="mailto:<?= $site->email()->html() ?>" title="<?= $site->author()->html() ?> eine E-Mail schreiben"><?= $site->email()->html() ?></a></span>
    </div>
    <div class="page-header__logo">
      <a href="<?= $site->url() ?>" title="Die Startseite anzeigen"><?php snippet("svg/logo") ?></a>
    </div>
  </div>
  <div class="page-header__toggle toggle-menu" id="toggle-menu">
    <div class="burger-badge">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <span class="page-header__toggle__menu">Menü</span>
    <span class="page-header__toggle__back">Zurück</span>
  </div>
</header>
