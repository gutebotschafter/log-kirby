<?php if($page->hideAside() != 'true'): ?>
  <aside class="page-aside">
    <?php foreach($kirby->collection('projects')->listed()->shuffle()->limit(1) as $item): ?>
      <section class="page-aside__teaser">
        <h2><a href="<?= $item->parent()->url() ?>" title="Alle Projekte anzeigen">Aktuelle Projekte.</a></h2>
        <a href="<?= $item->url() ?>" title="Das Projekt <?= $item->title()->html() ?> anzeigen">
          <figure>
            <img src="<?= $item->picture()->toFile()->thumb(['width' => 480, 'height' => 320, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $item->picture()->toFile()->alt()->html() ?>">
          </figure>
          <p>
            <strong><?= $item->title()->html() ?></strong>
            <?= $item->description()->html() ?>
          </p>
        </a>
      </section>
    <?php endforeach ?>
    <?php foreach($kirby->collection('employees')->listed()->shuffle()->limit(1) as $item): ?>
      <section class="page-aside__teaser">
        <h2><a href="<?= $item->parent()->url() ?>" title="Alle im Team anzeigen">Wir stellen uns vor.</a></h2>
        <a href="<?= $item->url() ?>" title="<?= $item->title()->html() ?> anzeigen">
          <figure>
            <img src="<?= $item->picture()->toFile()->thumb(['width' => 480, 'height' => 320, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $item->picture()->toFile()->alt()->html() ?>">
          </figure>
          <p>
            <strong><?= $item->title()->html() ?></strong>
            <?= $item->description()->html() ?>
          </p>
        </a>
      </section>
    <?php endforeach ?>
    <?php foreach($kirby->collection('testimonials')->listed()->shuffle()->limit(1) as $item): ?>
      <section class="page-aside__teaser">
        <h2>Andere über uns.</h2>
        <blockquote>
          <?= $item->blockquote()->kt() ?>
          <cite><?= $item->cite()->html() ?></cite>
        </blockquote>
      </section>
    <?php endforeach ?>
  </aside>
<?php endif ?>
