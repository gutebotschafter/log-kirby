<?php

  $coverImage = '';
  $covertext  = '';
  $coverBlur  = '';

  // get cover image and blur

  if($page->coverImage()->isNotEmpty()) {
    $coverImage = $page->coverImage()->toFile();
    $coverBlur = $page->coverBlur()->value();
  }

  elseif($page->parent() and $page->parent()->coverImage()->isNotEmpty()) {
    $coverImage = $page->parent()->coverImage()->toFile();
    $coverBlur = $page->parent()->coverBlur()->value();
  }

  // get the cover text

  $coverText = $site->claim()->html();

  if($page->coverText()->isNotEmpty()) {
    $coverText = $page->coverText()->html();
  }

  elseif($page->parent() and $page->parent()->coverText()->isNotEmpty()) {
    $coverText = $page->parent()->coverText()->html();
  }

?>

<?php if ($coverImage != ''): ?>

  <div class="page-cover" style="background-image: url('<?= $coverImage->thumb(['width' => 1440, 'height' => 720, 'crop' => true, 'quality' => 90, 'blur' => $coverBlur])->url() ?>');">
    <div class="page-cover__text">
      <?= $coverText->widont() ?>
    </div>
    <a class="page-cover__seal" href="https://www.dgsf.org/zertifizierung/dgsf-empfohlene-einrichtungen" rel="noopener">
      <img src="<?= url('assets/images/seal.png') ?>" alt="Durch den DGSF empfohlene Einrichtung">
    </a>
  </div>

<?php endif ?>
