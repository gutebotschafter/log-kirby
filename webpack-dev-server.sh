#!/usr/bin/env bash

DOCKER_NGINX=$(docker ps | grep _web_ | cut -d' ' -f1)

docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html && npm start"
