import { addEventListenerOnce } from '../utils/event-listener';

let $navigation = null;
let $dropdowns = null;
let opened = false;

/**
 * Removes active circles
 *
 * @returns {void}
 */
const removeActiveCircles = () => {
    Array.prototype.forEach.call($dropdowns, $dropdown => {
        const $circle = $dropdown.querySelector('.circle-plus');
        if ($circle) {
            $circle.classList.remove('opened');
        }

        const $submenu = $dropdown.querySelector('.dropdown.submenu');
        if ($submenu) {
            $submenu.classList.remove('dropdown--open');
        }
    });
};

/**
 * Animates the dropdown
 *
 * @returns {void}
 */
const dropdownAnimate = () => {
    $dropdowns = document.querySelectorAll('.page-menu__pages__dropdown');

    Array.prototype.forEach.call($dropdowns, $dropdown => {
        const $arrow = $dropdown.querySelector('.dropdown-arrow__wrapper');

        $arrow.addEventListener('click', () => {
            const $circlePlus = $arrow.parentNode.querySelector(
                '.circle-plus'
            );
            const activeCircle = $circlePlus.classList.contains('opened');
            const $submenu = $dropdown.querySelector('.dropdown.submenu');

            removeActiveCircles();

            if ($circlePlus && !activeCircle) {
                $circlePlus.classList.add('opened');
            }

            if ($submenu && !activeCircle) {
                $submenu.classList.add('dropdown--open');
            }
        });
    });
};

/**
 * Eventlistener for sidr closing
 *
 * @returns {void}
 */
const eventListener = () => {
    const $burger = $navigation.querySelector('.page-menu__burger');
    const $badge = $burger && $burger.querySelector('#burger-badge');
    const $overlay = $navigation.querySelector('.page-menu__overlay');
    const $wrapper = $navigation.querySelector('.page-menu__wrapper');

    if ($burger && $badge && $overlay) {
        $burger.addEventListener('click', () => {
            if (opened) {
                addEventListenerOnce($overlay, 'transitionend', () => {
                    $overlay.style.display = 'none';
                });

                $navigation.classList.remove('page-menu--show');
                $badge.classList.remove('open');
                $overlay.classList.remove('page-menu__overlay--show');

                removeActiveCircles();

                opened = false;
            } else {
                $badge.classList.add('open');
                $overlay.style.display = 'block';

                window.requestAnimationFrame(() =>
                    $overlay.classList.add('page-menu__overlay--show')
                );

                $navigation.classList.add('page-menu--show');

                opened = true;
            }
        });
    }
};

/**
 * Initiliaze the mobile navigation
 *
 * @returns {void}
 */
export const init = () => {
    $navigation = document.querySelector('.page-menu');

    if ($navigation) {
        dropdownAnimate();
        eventListener();
    }
};
