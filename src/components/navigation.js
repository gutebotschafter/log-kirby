let $navbar = null;
let $dropdowns = null;
let $header = null;

/**
 * Hides the navigation overlay
 *
 * @returns {void}
 */
const removeActiveSubNavigation = () => {
  const $selected =
    $navbar && $navbar.querySelectorAll(".dropdown.active");

  Array.prototype.forEach.call($selected, $item =>
    $item.classList.remove("active")
  );

  Array.prototype.forEach.call($dropdowns, $item =>
    $item.classList.remove("hover")
  );
};

/**
 * Returns top position from header
 *
 * @returns {string}
 */
const getBoundingClientRect = () =>
  `${$header.getBoundingClientRect().height +
    $header.getBoundingClientRect().top +
    $navbar.getBoundingClientRect().height}px`;

/**
 * Handles hover and click on navigation
 *
 * @param $item
 * @returns {void}
 */
const handleActivation = $item => {
  const $dropdown = $item.querySelector(".dropdown");
  const $link = $item.querySelector("a");

  if (!$link) {
    return;
  }

  $link.addEventListener("mouseenter", () => {
    removeActiveSubNavigation();

    if ($dropdown && $item.classList.contains("page-menu__pages__dropdown")) {
      $dropdown.style.top = getBoundingClientRect();
    }

    $item.classList.add("hover");
  });

  $navbar.addEventListener("mouseleave", () => removeActiveSubNavigation());
};

/**
 * Updates top position on scroll event
 *
 * @returns {void}
 */
const handleScrollEvent = () => {
  const $activeDropdown = $navbar && $navbar.querySelector(".submenu.active");

  if ($activeDropdown) {
    $activeDropdown.style.top = getBoundingClientRect();
  }
};

/**
 * Initiliaze the dropdown navigation
 *
 * @returns {void}
 */
export const init = () => {
  $header = document.querySelector(".page-header");
  $navbar = document.querySelector(".page-menu");
  $dropdowns =
    $navbar &&
    $navbar.querySelectorAll(
      ".page-menu__pages__item:not(.page-menu__pages__item--highlight)"
    );

  if ($dropdowns) {
    Array.prototype.forEach.call($dropdowns, $item => handleActivation($item));

    document.addEventListener("scroll", () => handleScrollEvent());
  }
};
