# KIRBY-CMS with docker, composer, twig and webpack-dev-server

## Table of Contents

  - [Installation](#installation)
  - [Usage](#usage)
    - [Container](#container)
    - [composer](#composer)
    - [npm](#npm)
    - [webpack](#webpack)
  - [Deployment](#deployment)
  - [Update](#update)
  - [Configuration](#configuration)
    - [Twig](#twig)
    - [Twig Cache](#twig-cache)
    - [Custom Template Functions](#custom-template-functions)
  - [Debugging with xdebug](#debugging-with-xdebug)
  - [Composer CLI Scripts](#composer-cli-scripts)
  - [Codestyling](#codestyling)
    - [PHP](#php)
    - [Javascript](#javascript)
  - [Local tools](#local-tools)

## Installation

Please make sure, that you installed the [local tools](#local-tools). If you have problems with **composer** see [here](#composer).

Run `docker-compose build` to pulling the image and building the container.

## Usage

First - Open the terminal and change to the project directory.
To start the docker-container run the command `docker-compose up`. (Shows log output)
If you want to start the container in the Background use `docker-compose up -d`.

### Container
To list the container use `docker ps`, you need the web_1 container name. Example:

| CONTAINER ID  | IMAGE        | COMMAND                | CREATED    | STATUS        | PORTS                  | NAMES                        |
| ------------- | ------------ | ---------------------- | -----------| --------------| -----------------------| -----------------------------|
| 0210fb635294  | nginx:latest | "nginx -g 'daemon of…" | 1 days ago | Up 34 minutes | 0.0.0.0:80->80/tcp,... | kirby-composer-webpack_web_1 |
| 98cb4db231e8  | php:7-fpm    | "docker-php-entrypoi…" | 1 days ago | Up 34 minutes | 9000/tcp               | kirby-composer-webpack_php_1 |

### composer

Run locally `composer install` to install all dependencies, if you have composer installed. 

Otherwise you can connect to the php container (like [npm](#npm))
and run the **composer.phar** with
```php
php composer.phar install
```

### npm

If the container is up, you must connect to the `web` container with the command
`docker exec -it <container-name> /bin/bash`. Ex.: `docker exec -it kirby-composer-webpack_web_1 /bin/bash`. 

Or you can connect to the container with the bash script `docker-connect.sh`.
Execution: `./docker-connect.sh [container]` - ex.: `./docker-connect.sh web`

Now you are connect over ssh to the Container. Switch to the `/var/www/html` directory and run `npm i` to install all node modules.

### webpack

Connect to the container and switch to the document root `/var/www/html` and execute the command `npm start` to start the webpack-dev-server. The javascript entry point can be found in /src/app.js

## Deployment

If the production server doesn't have npm installed, remove the assets directory `/public/assets` from the gitignore and commit after you have run `npm run build` (for Production the assets).

## Update

Update composer modules only over composer and node modules over npm!

## Configuration

### Twig

To configure twig, you can set the options in `/site/config/config.php`.
See details at https://github.com/mgfagency/kirby-twig/tree/master/doc

### Twig Cache

By Default, the cache is disabled. Also you can disable or enable the twig cache in the `/site/config/config.php` file.
```php
'mgfagency.twig.cache' => true
```
The cache directoy is located here `/site/cache/twig`.
To clear the cache, use the composer run script [here](#composer-cli-scripts).

### Custom Template Functions

Add your custom functions in the `/site/config/config.php` file. Ex. 
```php
'mgfagency.twig.function.customFunction' => 'customFunction'
```

### Debugging with xdebug

You can debug over xdebug directly into the container. In order to debug the php on the container, you must setup a new local ip like
```
sudo ifconfig en0 alias 10.254.254.254 255.255.255.0
```
Now you can debug with xdebug on port **9001** with the ip **10.254.254.254**. This ist important for the container to share xdebug data to the host machine.

## Composer CLI Scripts

To run composer scripts, try this like `composer run-script <script>` in the project root directoy, ex. `composer run-script cache:clear`

* Twig Cache Clear => `cache:clear`

## Codestyling

### php

For php codestyling follow the [PSR/2 Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)

### Javascript

This package includes an ESLint config (**.eslintrc**, from [Airbnb](https://github.com/airbnb/javascript)) and a prettier config (**.prettierrc**), to format the javascript code.

Activate the Plugins in your favourite IDE.

## Local tools

Make sure, that you have installed the following tools:

- docker ce (https://hub.docker.com/editions/community/docker-ce-desktop-mac)
- composer (https://pilsniak.com/install-composer-mac-os/)
